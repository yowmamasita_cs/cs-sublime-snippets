# cs-sublime-snippets

Collection of snippets commonly used at Cloud Sherpas GBU

## Install

`cd ~ && wget https://bitbucket.org/yowmamasita_cs/cs-sublime-snippets/raw/master/install.sh && bash install.sh`

## List

* [Ferris2](https://github.com/jonparrott/Ferris2)
    * model `f2m`
    * controller `f2c`
    * message `f2me`
    * *behavior - TODO*
    * *component - TODO*
* [Angular App Bootstrap](https://bitbucket.org/cloudsherpas/angular-app-bootstrap)
    * *controller - TODO*
    * *directive - TODO*
    * *service/factory - TODO*
    * *filter - TODO*
