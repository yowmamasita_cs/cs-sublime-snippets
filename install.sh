#!/bin/bash

set -e

echo 'Installing cs-sublime-snippets...'

git clone https://bitbucket.org/yowmamasita_cs/cs-sublime-snippets.git
cd ~/.config/sublime-text-3/Packages/
ln -s ~/cs-sublime-snippets

echo 'Done.'
